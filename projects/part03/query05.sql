select name, gradefrom Highschooler, (
    select ID1 from Friend
    except
        select distinct Friend.ID1
            from Friend, Highschooler S1, Highschooler S2
    where Friend.ID1 = S1.ID 
        and Friend.ID2 = S1.ID 
        and S1.grade = S2.grade) as Student
    where Highschooler.ID = Student.ID1;    
