.read data.sql

select  distinct S1.name, S1.grade from highschooler as S1
    join Friend as F on F.ID1 = S1.ID
    join Highschooler as S2 on S2.ID = F.ID2
    where S1.grade = S2.grade
order by S1.grade, S1.name
