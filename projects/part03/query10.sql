.read data.sql

select S1.name, S1.grade, S2.name, S2.grade from Likes L1, Highschooler S1, Highschooler S2
    where L1.ID1 = S1.ID 
    and L1.ID2 = S2.ID
    and (S1.grade-S2.grade) >= 2;