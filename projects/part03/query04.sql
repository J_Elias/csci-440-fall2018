select S1.name, S1.grade, S2.name, S2.grade, S3.name, S3.grade
from Likes L1, Likes L2, Highschooler S1, Highschooler S2, Highschooler S3
    where L1.ID2 = L2.ID1
    and L2.ID2 != L1.ID1
    and L1.ID1 = S1.ID and L1.ID2 = S2.ID and L2.ID2 = S3.ID;