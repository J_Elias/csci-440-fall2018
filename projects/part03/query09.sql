.read data.sql

select name from Highschooler
    where ID in 
        (select ID1 from Friend
            where Friend.ID1 in 
                (select ID from Highschooler
                    where name = 'Gabriel') 
            or Friend.ID2 in 
                (select ID from Highschooler
                    where name = 'Gabriel'))
    or ID in 
        (select ID2 from Friend
            where Friend.ID1 in 
                (select ID from Highschooler
                    where name = 'Gabriel')
            or Friend.ID2 in 
                (select ID from Highschooler
                    where name = 'Gabriel'))
    except
        select name from Highschooler
            where name = 'Gabriel';
